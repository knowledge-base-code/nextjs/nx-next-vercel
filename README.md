# Nx-Next-Vercel

## Playthrough

Link to Nx with Next: https://nx.dev/nx-api/next

```
gp init
npx create-nx-workspace@latest --preset=next
```

Creates `org` monorepo with project `fedorko`.

## Run the project
```shell
npx nx run fedorko:serve
```